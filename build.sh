#!/bin/bash

# Build script for building qt for ARM

set -e

cd $(dirname $0)

DEFAULT_TOOLCHAIN_PATH=~/ti-processor-sdk-linux-am335x-evm-02.00.01.07/linux-devkit/sysroots/x86_64-arago-linux/usr/bin
DEFAULT_SYSROOT_PATH=~/ti-processor-sdk-linux-am335x-evm-02.00.01.07/linux-devkit/sysroots/cortexa8hf-vfp-neon-linux-gnueabi
#PATH_TO_SYSROOT=${TOOL_CHAIN_PATH%x86_64-arago-linux/usr/bin}cortexa8hf-vfp-neon-linux-gnueabi
TSLIB_PATH_LIB=../rh_tslib/target_output/target/usr/lib
TSLIB_PATH_INCLUDE=../rh_tslib/target_output/usr/include
ARCHITECTURE=arm
CROSS_COMPILER=arm-linux-gnueabihf-
NUM_ARGS="$#"

if [ $NUM_ARGS -eq 0 ]
then
	echo "No argments passed, building with:"
	echo "default toolchain path: "$DEFAULT_TOOLCHAIN_PATH""
	echo "default sysroot path: "$DEFAULT_PATH_TO_SYSROOT""
	echo ""
	TOOL_CHAIN_PATH="$DEFAULT_TOOLCHAIN_PATH"
    PATH_TO_SYSROOT="$DEFAULT_SYSROOT_PATH"
elif [ $NUM_ARGS -eq 2 ]
then
	TOOL_CHAIN_PATH="$1"
	PATH_TO_SYSROOT="$2"
else
	echo "Script only handles working with defaults or having paths to both tool chain and sysroots passed in, exiting"
	exit 1
fi

if [ ! -e "$TOOL_CHAIN_PATH"/arm-linux-gnueabihf-gcc ]
then
    echo "Toolchain path "$TOOL_CHAIN_PATH""
	echo "does not appear to contain the correct tools, exiting"
    echo ""
    exit 1
fi

if [ ! -d "$PATH_TO_SYSROOT" ]
then
	echo "Sysroot path: "$PATH_TO_SYSROOT""
	echo "appears to be incorrect, exiting"
	exit 1
fi

if [ ! -e "$TSLIB_PATH_LIB"/libts.so ]
then
	echo "tslib must be built before Qt, exiting"
	exit 1
fi

TSLIB_PATH_LIB=$(readlink -f "$TSLIB_PATH_LIB")

TSLIB_PATH_INCLUDE=$(readlink -f "$TSLIB_PATH_INCLUDE")

export PATH="$TOOL_CHAIN_PATH":"$PATH"
#export ARCH=arm
#export CROSS_COMPILE="$CROSS_COMPILER"

echo "#### Extracting Sources ####"
tar -xf qt-everywhere-opensource-src-5.6.0.tar.gz --checkpoint=.5000 
echo ""
echo ""

echo "#### Creating build folders ####"
echo ""

if [ -e host ]
then
	echo "remove host folder for a clean build, exiting"
	exit 1
else
	mkdir host
fi

if [ -e host-tools ]
then
	echo "host-tools folder exists, moving on"
	echo ""
else
	mkdir host-tools
fi

if [ -e target ]
then
	echo "target folder exists, moving"
	echo ""
else
	mkdir target
fi

echo "#### Moving device folder ####"
echo ""

if [ -e qt-everywhere-opensource-src-5.6.0/qtbase/mkspecs/devices/linux-rite-hite-g++ ]
then
 	echo "device folder exists moving on"
	echo ""
else
	cp -r linux-rite-hite-g++ qt-everywhere-opensource-src-5.6.0/qtbase/mkspecs/devices/
fi

# Move to host folder to keep source clean, clean builds can now be performed by deleting host folder
# before running this script
cd host

PATH_TO_CONFIGURE=../qt-everywhere-opensource-src-5.6.0/configure
PATH_TO_HOST_TOOLS=$(readlink -f ../host-tools)
PATH_TO_TARGET=$(readlink -f ../target)/qt5

echo "#### Configure build ####"
echo ""

( exec "$PATH_TO_CONFIGURE" -v -opensource -confirm-license -device linux-rite-hite-g++ \
-device-option CROSS_COMPILE=arm-linux-gnueabihf- -sysroot "$PATH_TO_SYSROOT" \
-prefix /opt/qt5 -hostprefix "$PATH_TO_HOST_TOOLS" -extprefix "$PATH_TO_TARGET" \
-no-sse2 -no-mtdev -no-cups -no-largefile -no-openssl -no-gtkstyle -no-sql-db2 \
-no-sql-ibase -no-sql-mysql -no-sql-oci -no-sql-odbc -no-sql-psql -no-sql-sqlite \
-no-sql-sqlite2 -no-sql-tds -no-libproxy -no-libinput -no-xcb-xlib -qt-pcre -no-openvg \
-no-xinput2 -no-xkbcommon-evdev -no-xcb -no-xrender -skip qtwebview -skip qtwebengine -release -I "$TSLIB_PATH_INCLUDE" \
-L "$TSLIB_PATH_LIB" )


echo "#### Building ####"
echo ""

make -j8

echo "#### Installing ####"
echo ""

make install

echo "Qt build - COMPLETE"
echo ""

exit 0
